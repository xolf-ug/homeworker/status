import App from "./App.svelte";

let serviceWorkerAvailable: boolean = false

const registerServiceWorker = (): Promise<void> => new Promise(resolve => {
    if(!("serviceWorker" in navigator)) return resolve()

    navigator.serviceWorker.register("/serviceworker.js", { "scope": "/" })
    .then(() => {
        serviceWorkerAvailable = true
        resolve()
    })
    .catch(error => {
        console.error(error)
        resolve()
    })
})

/* Fancy stuff */
;(async () => {
    await registerServiceWorker()

    new App({
        target: document.body,
        props: {
            serviceWorkerAvailable,
        },
    })
})()
type downtime = {
    "id": string,
    "error": string,
    "started_at": string,
    "ended_at": string,
    "duration": number
}

export default downtime
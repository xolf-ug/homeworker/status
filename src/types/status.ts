type entry = {
    "token": string,
    "url": string,
    "alias": string,
    "last_status": number,
    "uptime": number,
    "down": boolean,
    "down_since": number | boolean,
    "error": any,
    "period": number,
    "apdex_t": number,
    "string_match": any,
    "enabled": boolean,
    "published": boolean,
    "disabled_locations": any,
    "last_check_at": string,
    "next_check_at": string,
    "mute_until": number | boolean,
    "favicon_url": string | number,
    "custom_headers": any,
    "http_verb": string,
    "http_body": any,
}
type status = Array<entry>

export type { status, entry }
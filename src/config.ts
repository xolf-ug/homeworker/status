export const UPDOWN_KEY = "ro-eMHE9uCaDWL7XD8vy72G"

export const URL = "https://homeworker.li"

export const STATUS_URL = `https://updown.io/api/checks?api-key=${UPDOWN_KEY}`
export const INCIDENTS_URL = "https://gitlab.com/api/v4/projects/13728281/issues?state=opened&labels=incident&order_bycreated_at"

export const NOTIFICATIONS = {
    STATUS: "/api/v1/notifications/status",
    ACTIVATE: "/api/v1/notifications/activate",
    DEACTIVATE: "/api/v1/notifications/deactivate",
}
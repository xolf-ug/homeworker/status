export default {
    "4caf": ["Dateien", "Bereitstellung der im Homeworker gespeicherten Dateien"],
    "cekd": ["Status", "Überwachung des Status & der Uptime der Systeme"],
    "24g5": ["API (v1)", "API v1 (Kommunikation unter den Komponenten, legacy)"],
    "wpgs": ["API (v2)", "API v2 (Schülerportal, Chats, Hausaufgaben)"],
    "4va1": ["App", "Web & Desktop sowie Native-apps"],
    "1tl8": ["Anmeldung", "Anmeldung & Authentifizierung von Nutzern"],
    "upcr": ["Startseite", "Statische Seiten (Landing-Pages, Marketingseiten, Kontaktseite)"],
}

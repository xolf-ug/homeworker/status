const INCIDENT_URL = "https://gitlab.com/api/v4/projects/13728281/issues?state=opened&labels=incident&order_bycreated_at"
const init = {
    "status": 200,
    "statusText": "ok",
}
let sendNotifications = false
let interval = null

let notified = []

const startService = () => new Promise(async resolve => {
    const current = await (await fetch(INCIDENT_URL)).json()
    current.forEach(incident => notified.push(incident.id))

    self.registration.showNotification("Nachrichten aktiviert")

    interval = setInterval(checkForUpdates, 1000 * 60 * 30) // 30 Minutes
    sendNotifications = true
    resolve()
})
const stopService = () => {
    notified = []
    clearInterval(interval)

    sendNotifications = false
}
const checkForUpdates = async () => {
    const incidents = await (await fetch(INCIDENT_URL)).json()
    const tmp = []

    incidents.forEach(incident => {
        if(!notified.includes(incident.id)) {
            self.registration.showNotification(incident.title, {
                body: incident.description,
                icon: incident.author.avatar,
                timestamp: new Date(incident.created_at).getTime(),
            })
        }
        tmp.push(incident.id)
    })

    notified = tmp
}

self.addEventListener("fetch", async event => {
    const url = new URL(event.request.url)

    if(event.request.method != "GET") return
    if(!/\/api\/v1\/notifications\/+/.test(url.pathname)) return

    if(/\/api\/v1\/notifications\/status/.test(url.pathname)) {
        event.respondWith(new Response(JSON.stringify({
            "status": "success",
            "body": {
                "active": sendNotifications,
            },
        }), init))
    } else if(/\/api\/v1\/notifications\/activate/.test(url.pathname)) {
        if(self && self.registration && self.registration.showNotification) {
            event.respondWith(new Response(JSON.stringify({ "status": "success", }), init))
            await startService()
        }
    } else if(/\/api\/v1\/notifications\/deactivate/.test(url.pathname)) {
        event.respondWith(new Response(JSON.stringify({ "status": "success", }), init))
        stopService()

    // For Development
    } else if(/\/api\/v1\/notifications\/force/.test(url.pathname)) await checkForUpdates()
})